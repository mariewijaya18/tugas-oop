<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");
echo "Name : ". $sheep->name . "<br>";
echo "Legs : ". $sheep->legs . "<br>";
echo "Cold Blooded : ". $sheep->cold_blooded . "<br><br>";

$kodok = new frog('Buduk');
echo "Name : ".$kodok->name . "<br>";
echo "Legs : ".$kodok->legs . "<br>";
echo "Cold Blooded : ".$kodok->cold_blooded . "<br>";
$kodok->suara();
echo "<br>";
echo "<br>";

$sungokong = new ape('Kera Sakti');
echo "Name : ".$sungokong->name . "<br>";
echo "Legs : ".$sungokong->legs . "<br>";
echo "Cold Blooded : ".$sungokong->cold_blooded . "<br>";
$sungokong->jump();